package jcd.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import jcf.components.AppDataComponent;
import jcf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    ArrayList<UMLClass> classList;
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        classList = new ArrayList();
    }
    
    public void createUMLClass(double x, double y){
        UMLClass nClass = new UMLClass(x,y);
        classList.add(nClass);
    }
    public ArrayList<UMLClass> getClassList(){
        return classList;
    }
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {

    }
}

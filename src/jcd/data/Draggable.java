/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

/**
 *
 * @author Garry Huang
 */
public interface Draggable {
    public void start(int x, int y);
    public void drag(int x, int y);
    public void setLocation(double initX, double initY);
}

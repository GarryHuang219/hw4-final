/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.scene.text.Text;

/**
 *
 * @author Garry Huang
 */
public class UMLClass extends Text implements Draggable{
    String className;
    String packageName;
    String parent;
    double startX;
    double startY;
    ArrayList<String> varName;
    ArrayList<String> varType;
    ArrayList<Boolean> varStatic;
    ArrayList<String> varAccess;
    
    ArrayList<String> metName;
    ArrayList<String> metReturn;
    ArrayList<Boolean> metStatic;
    ArrayList<Boolean> metAbstract;
    ArrayList<String> metAccess;
    ArrayList<String> arg;
    
    public UMLClass(double initX, double initY){
        setX(initX);
        setY(initY);
    }

    @Override
    public void start(int x, int y) {
        startX = x;
        startY = y;
        setX(x);
        setY(y);
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - (getX());
	double diffY = y - (getY());
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
	startX = x;
	startY = y;
    }
    @Override
    public void setLocation(double initX, double initY) {
        xProperty().set(initX);
	yProperty().set(initY);
    }

}
